#!/usr/bin/python
import pika
import sys
print "Content-Type: text/html\r\n\r\n"

credentials = pika.PlainCredentials('tpigram', 'newpass')
connection = pika.BlockingConnection(pika.ConnectionParameters("52.10.158.23", 5672, '/', credentials))
channel = connection.channel()

# channel.exchange_declare(exchange='', type='fanout')

channel.queue_declare(queue='new_queue')

message = '{example}'
channel.basic_publish(exchange='',
                      routing_key='new_queue',
                      body=message)
print message
connection.close()