#!/usr/bin/env python
import pika
print "Content-Type: text/html\r\n\r\n"

credentials = pika.PlainCredentials('customer', 'customer')
connection = pika.BlockingConnection(pika.ConnectionParameters("52.10.158.23", 5672, '/', credentials))
channel = connection.channel()

channel.exchange_declare(exchange='logs',
                         type='fanout')

# result = channel.queue_declare(exclusive=True)
queue_name = 'new_test_consumer1'

channel.queue_bind(exchange='logs',
                   queue=queue_name)

# print ' [*] Waiting for logs. To exit press CTRL+C'

def callback(ch, method, properties, body):
    print " [x] %r" % (body,)

channel.basic_consume(callback,
                      queue=queue_name,
                      no_ack=True)

# channel.start_consuming()